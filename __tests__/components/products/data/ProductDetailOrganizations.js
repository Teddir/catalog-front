module.exports = {
  organizations: {
    data: {
      organizations: [
        {
          id: 2,
          name: 'Another Organization',
          imageFile: 'fake-image.png',
          slug: 'ao',
          __typename: 'Organization'
        },
      ]
    }
  }
}
