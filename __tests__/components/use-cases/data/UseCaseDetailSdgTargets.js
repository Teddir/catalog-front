export const sdgTargets = {
  data: {
    sdgTargets: [{
      id: 3,
      name: 'Another SDG Target',
      targetNumber: '3',
      slug: 'as',
      sustainableDevelopmentGoal: {
        slug: 'sdg_target_slug_3'
      }
    }]
  }
}

